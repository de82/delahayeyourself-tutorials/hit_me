<?php require "vendor/autoload.php";

$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/templates');
$twigConfig = array(
    'debug' => true,
);

Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
    $twig->addFilter(new Twig_Filter('markdown', function($string){
        return renderHTMLFromMarkdown($string);
    }));
});


class MyDB extends SQLite3
{
    function __construct()
    {
        $this->open('test.sqlite3');
    }
}

Flight::before('start', function(&$params, &$output){
    
});


Flight::map('render', function($template, $data=array()){
    Flight::view()->display($template, $data);
});


Flight::route('/', function(){
    Flight::render('index.html');
});

Flight::route('/XSS', function(){
    $data = [
        "comments" => [
            "Lorem ipsum",
            "Lorem ipsum bis",  
        ],
    ];
    if(Flight::request()->method == 'POST'){
        $data["comments"][] = Flight::request()->data->comment;
    }
    Flight::render('xss.html', $data);
});

Flight::route('/SQLI', function(){
    $data = [
        "login" => false,
        "sql" => null,
    ];
    if(Flight::request()->method == 'POST'){
        $username = Flight::request()->data->username;
        $password = Flight::request()->data->password;
        
        $query = "SELECT id FROM users WHERE username = '$username' AND password = '$password'";
        $data['sql'] = $query;
        
        $db = new MyDB();
        $result = $db->query($query);
        $result = $result->fetchArray();

        if($result == false)
        {
            $data['error'] = true;
        }
        else
        {
            $data['login'] = true;
        }
    }
    Flight::render('sqli.html', $data);
});

Flight::start();